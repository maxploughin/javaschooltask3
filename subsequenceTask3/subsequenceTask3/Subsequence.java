package subsequenceTask3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Subsequence {
    public boolean Subsequence (List mainArray, List secondArray){

        for (int i = 0; i < mainArray.size(); i++){
            if (!secondArray.contains(mainArray.get(i))){
                return false;
            }

            for (int j = 0; j < secondArray.size(); j++) {
                var forCheck = secondArray.get(j);
                if (!mainArray.contains(forCheck)) {
                    secondArray.set(j, "");

                    continue;
                }
            }
        }

        return true;
    }
}
