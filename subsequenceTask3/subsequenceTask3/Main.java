package subsequenceTask3;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Subsequence s = new Subsequence();


        boolean b = s.Subsequence(Arrays.asList("A", "B", "C", "D"),
                Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
        System.out.println(b);
    }
}
